"use strict";
// Задание

// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.
// Создайте геттеры и сеттеры для этих свойств.
// Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).
// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.
// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.
class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }
  get personName() {
    return `${this.name}`;
  }
  get personAge() {
    return `${this.age}`;
  }
  get personSalary() {
    return `${this.salary}`;
  }
  set personName(newName) {
    this.name = newName;
  }
  set personAge(newAge) {
    this.age = newAge;
  }
  set personSalary(newSalary) {
    this.salary = newSalary;
  }
}
const hero = new Employee();
console.log((hero.personName = "Ira"));
console.log((hero.personAge = 23));
console.log(hero);
//
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get personSalary() {
    return `${this.salary}` * 3;
  }
}
const ninjaJs = new Programmer("ninja", 18, 1000, {
  ua: "UK-language",
  ru: "RU-language",
  en: "EN-language",
});
console.log(ninjaJs);
console.log(ninjaJs.personSalary);

//
const frontEndAdmin = new Programmer("Gloria", 28, 10000, {
  js: "javascript",
  nodeJs: "nodeJs",
  react: "react-js",
  angular: "angular",
});

console.log(frontEndAdmin);
//
const backEndAdmin = new Programmer("dima", 32, 2000, {
  php: "php",
});

console.log(backEndAdmin);
